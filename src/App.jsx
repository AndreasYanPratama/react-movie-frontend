// ./src/App.jsx
import React from 'react';
// import Menu from './components/Menu';
// import data from './data/recipes';

// pages
// import Home from './pages/Home';
import AddPost from './pages/posts/AddPostPage';

import MoviesList from './pages/movies/MoviesList';

// function App() {
//   return (
//     <Menu recipes={data} />
//   );
// }

// function App() {
//   return (
//     <Home />
//   );
// }

// function App() {
//   return (
//     <AddPost />
//   );
// }

function App() {
  return (
    <MoviesList />
  );
}

export default App;
