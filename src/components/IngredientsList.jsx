// ./src/components/IngredientsList.jsx
import React from 'react';
import Ingredient from './Ingredient';

function IngredientsList({ list }){
    return(
        <ul className="ingredients">
            {list.map((ingredient, i) => (
                <Ingredient 
                    key={i} 
                    amount={ingredient.amount}
                    measurement={ingredient.measurement}
                    name={ingredient.name} />
            ))}
        </ul>
    );
}
export default IngredientsList;