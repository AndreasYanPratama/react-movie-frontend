import React, { useState } from "react";
import { FaStar } from "react-icons/fa";

// helper function
function createArray(length){
    return[...Array(length)];
}

function StarRating({ totalStars = 5 }){
    const [selectedStars, setSelectedStars] = useState(0);

    const StarItem = ({
        selected = false,
        // when user click on a star it will setting new value for the stars
        onSelect = (param) => param,
    }) => (
        <FaStar color={selected ? 'red' : 'gray'} onClick={onSelect} />
    );

    const renderStarItem = () => createArray(totalStars).map((number,key) => (
        <StarItem selected={selectedStars > key} key={key} onSelect={() => setSelectedStars(key + 1)} />
    ));

    // return [
    //     <FaStar color="red" />,
    //     <FaStar color="red" />,
    //     <FaStar color="red" />,
    //     <FaStar color="grey" />,
    //     <FaStar color="grey" />
    // ];

    return (
        <>
            {renderStarItem()}
            <p>
                {`${selectedStars} of ${totalStars} stars`}
            </p>
        </>
    );
}
export default StarRating;