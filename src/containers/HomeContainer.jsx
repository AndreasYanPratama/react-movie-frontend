// ./src/App.jsx
import React from 'react';

import StarRating from '../components/StarRating';

function HomeContainer(){
    return(
        <StarRating />
    );
}
export default HomeContainer;