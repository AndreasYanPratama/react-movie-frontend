// ./src/App.jsx
import React, { useState } from 'react';

function useCounter() {
  const [counter, setCounter] = useState(0);

  const showOddNumberOnly = () => {
    setCounter(counter % 2 === 0 ? counter : 0);
  };

  return {
    showOddNumberOnly,
    counter,
  };
}

function HomeContainer() {
  const [counter, setCounter] = useState(0);
  const [loading, setLoading] = useState(true);

  const handleClickMe = () => {
    setCounter(counter + 1);
    setLoading(false);

    if (counter === 5) {
      setLoading(true);
    }
  };

  return (
    <>
      <button type="button" onClick={handleClickMe}>
        Click Me
      </button>
      <p>
        Counting :
        {' '}
        {counter}
      </p>
      {
        loading ? <span>Loading...</span> : <span />
      }
    </>
  );
}

export default HomeContainer;