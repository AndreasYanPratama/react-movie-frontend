import React, { useEffect, useState} from 'react';

import './styles1.css';

// config
const CONFIG = {
    KEY: 'a226fc5f2b6941df49be678c6dda5db0',
    BASE_URL: 'https://api.themoviedb.org/3/',
    BASE_IMAGE_URL: 'https://image.tmdb.org/t/p/w500/',
    DEFAULT_LANGUAGE: 'en-us',
};

// api helpers
const API_ENDPOINT = {
    NOW_PLAYING: `${CONFIG.BASE_URL}movie/now_playing?api_key=${CONFIG.KEY}&language=${CONFIG.DEFAULT_LANGUAGE}&page=1`,
    UPCOMING: `${CONFIG.BASE_URL}movie/upcoming?api_key=${CONFIG.KEY}&language=${CONFIG.DEFAULT_LANGUAGE}&page=1`,
    DETAIL: (id) => `${CONFIG.BASE_URL}movie/${id}?api_key=${CONFIG.KEY}`,
};

function MoviesListContainer(){
    const [movies, setMovies] = useState([]);

    const loadNowPlayingMovies = async () => {
        try {
            const response = await fetch(API_ENDPOINT.NOW_PLAYING, {
                method: 'GET',
            });
            const data = await response.json();
            // console.log(data.results)
            setMovies(data.results);
            // console.log(movies);
        }catch(error){
            Promise.reject(error);
        }
    }
    // load now playing movies
    useEffect(() => {
        loadNowPlayingMovies();
    }, []); 

    return(
        <>
        <header>
            <h3 className="sub-judul">Now Playing Movies</h3>
        </header>
        {/* <div>
        <h3 className="sub-judul">Shang-Chi</h3><br/><br/>
        </div> */}
        <main id="main">
            {
                movies.length > 0 && movies.map((movie, i) => (
                    <div className="movie" key={i}>
                        <img src={`${CONFIG.BASE_IMAGE_URL}/${movie.poster_path}`} alt="" />   
                        {/* <img src={`${CONFIG.BASE_IMAGE_URL}/${movie.backdrop_path}`} alt="" />   */}
                        <div className="movie-info">
                            <h3>{movie.title}</h3>
                            <span className="green">Rate <br/> {movie.vote_average}</span>
                        </div>
                        <div className="movie-date">
                            {movie.release_date}
                        </div>
                        <div className="overview">
                            <h3>Overview</h3>
                            {
                                movie.overview ? movie.overview : '- not yet available -'
                            }
                        </div>
                    </div>
                ))
            }
            {/* </div> */}
        </main>
        </>
    );

}
export default MoviesListContainer;