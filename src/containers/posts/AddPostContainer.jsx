// import React from 'react';

import React, { useState } from "react";
import './styles.css';

const MAX_CHARACTER = 10;

//helpers
function notFilled(value){
    return !value || value === null || value === undefined;
}

function maxCharacter(value, max){
    return value && value.length > max;
}

function AddPost(){
    const[title, setTitle] = useState(''); //required
    const[story, setStory] = useState(''); //max 1000 character

    // message from each inputs
    const [messageTitle, setMessageTitle] = useState('');
    const [messageStory, setMessageStory] = useState('');

    const handleChangeTitle = (e) => {
        setMessageTitle('');
        const inputValue = e.target.value;
        setTitle(inputValue);
    };
    
    const handleChangeStory = (e) => {
        setMessageStory('');
        const inputValue = e.target.value;
        if(maxCharacter(inputValue, MAX_CHARACTER)){
            setMessageStory(`Story must be at least than ${MAX_CHARACTER} characters`);
            e.target.value = inputValue.substring(0, MAX_CHARACTER);
            return;
        }
        setStory(inputValue);
    };

    const handleSubmit = (e) => {
        e.preventDefault();

        if(notFilled(title)){
            setMessageTitle('Title is required');
            return;
        }

        if(notFilled(story)){
            setMessageStory('Story is required');
            return;
        }

        // submit story data to an API
        const payload = {title, story}
        console.log('payload : ', payload);
    };

    return(
        <form className="container" onSubmit={(e) => handleSubmit(e)}>
            <div>
                <h1>Add Story</h1>
            </div>
            <div className="form-control">
                <label htmlFor="title" title="Title">
                    <span className="form-control__title">Title</span>
                    <input 
                        tabIndex="0"
                        name="title"
                        placeholder="Add Title"
                        type="text"
                        id="title" 
                        onChange={(e) => handleChangeTitle(e)}
                    />
                </label>
                {
                    messageTitle && (<span className="form-control__message error">{messageTitle}</span>)
                }
            </div>
            <div className="form-control">
                <label htmlFor="story" title="Story">
                    <span>Story</span>
                    <textarea 
                        tabIndex="0"
                        name="story" 
                        placeholder="Put your story here"
                        cols="10" 
                        rows="5" 
                        onChange={(e) => handleChangeStory(e)}
                    />
                    <span style={{ textAlign: 'right' }}>
                        {story.length}
                        /
                        {MAX_CHARACTER}
                    </span>
                </label>
                {
                    messageStory && (
                        <span className="form-control__message error">{messageStory}</span>
                    )
                }
            </div>
            <div>
                <button type="submit">Save Story</button>
            </div>
        </form>
    );

}
export default AddPost;