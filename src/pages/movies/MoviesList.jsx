import React, {useEffect} from 'react';
import MoviesListContainer from '../../containers/movies/MoviesListContainer';

function MoviesListPage(){
    // add title page
    useEffect(() => {
        document.title = 'Now Playing Movies';
    });

    return(
        <MoviesListContainer />
    );
}

export default MoviesListPage;