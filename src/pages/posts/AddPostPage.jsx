import React from 'react';

import AddPostContainer from '../../containers/posts/AddPostContainer';

function AddPostPage(){
    return(
        <>
            <AddPostContainer />
        </>
    );
}
export default AddPostPage