const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = {
    entry: path.resolve(__dirname, './src/index.jsx'), //file mana yg ingin jadi gerbang masuk
    output: { // file mana yg akan ditampilkan
      path: path.resolve(__dirname, './dist'),
      filename: 'bundle.js',
      clean: true,
    },
    resolve: {
      extensions: ['.js', '.jsx', '.json', '*'],
      modules: ['node_modules'],
    },
    devServer: { // utk menjalankan
      static: { 
        directory: path.resolve(__dirname, 'dist'),
      },
      port: 8000,
      hot:true // agar bisa reload otomatis
    },
  
    //ada yg harus di explore
    
    plugins: [
      new HtmlWebpackPlugin({
        template: path.resolve(__dirname, './src/index.html'),
        filename: 'index.html',
      }),
    ],

    module: {
        rules: [
            {
                test: /\.(js|jsx)$/,
                exclude: /node_modules/,
                use: {
                  loader: 'babel-loader',
                },
            },
            {
                test: /\.css$/i,
                use: [
                  'style-loader',
                  'css-loader',
                ],
            }
        ],
    },
    
};
  